## [0.0.5]

* точка и тире в названии cmpAction/cmpDataset

## [0.0.4]

* Немного расширил поддержку инлайновых cmptype
* Убрал некоторые баги

## [0.0.3]

* Базовая поддержка cmptype=Action, DataSet, Script
* Выкинул лишнее

## [0.0.2]

* Исправлен небольшой баг с тегами
* Исправлена документация

## [0.0.1]

* Первая публикация
* Базовая поддержка HTML разметки
* Подсветка cmpScript, cmpAction, cmpDataSet. Без поддержки cmpDataSetVar/cmpActionVar/cmpSubAction/cmptype