# О проекте

Создано в свободное от работы время, для работы в VS Code с формами с кастомной подсветкой.
Поддержка VS Code версии 1.38.0 +

***

## Что делать если нашел баг?

Поделитесь им со мной, связавшись https://gitlab.com/superseo/
Либо, собрав побольше информации, приступайте к созданию самостоятельного pull-reqeust с исправлением

***

## Как дебажить

В отдельном окне с формами используйте Cmd+Shift+P, выберите "Developer: Inspect TM Scopes".
Пока этот «режим» включен, то есть до тех пор, пока вы не нажмете клавишу Esc, вы можете перемещаться по файлу с помощью мыши или клавиатуры и проверять его, ища интересные области.
После изменения правил, выключить и включить обратно расширение в окне с формами.

***

## Как поделиться доработками

Склонируйте проект в папку с расширением.
Путь в Win10: %USERPROFILE%\\.vscode\extensions
Создайте и отправьте pull request в репозиторий
<https://gitlab.com/superseo/textmate-frm-language>

***

#### Полезные ссылки
Самая подробная на мой взгляд статья, о том как работает синтаксис TextMate
<https://www.apeth.com/nonblog/stories/textmatebundle.html>

Документация VS Code о языковых расширениях
<https://code.visualstudio.com/api/language-extensions/syntax-highlight-guide>

То же самое от Sublime Text
<https://www.sublimetext.com/docs/3/syntax.html>
<https://www.sublimetext.com/docs/3/scope_naming.html>

Хороший инструмент, с хорошей документацией, в т.ч. по синтаксису.
Но, к сожалению, реализовано на другом формате разметки.
<https://eeyo.io/iro/>
<https://eeyo.io/iro/documentation/>

Скрипт который худо бедно конвертирует plist to json, с ошибками. Полезно если нашел пример пакета, но в формате plist
<https://jsfiddle.net/stevetranby/X6SDU/>
